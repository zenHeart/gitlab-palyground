# gitlab pages 

## 概述
该示例说明如何使用 [gitlab pages](http://gitlab.com/help/user/project/pages/index.md)

gitlab pages 是 gitlab 提供的静态资源服务,你可以访问 <http://gitlab.com/pages/chengle297/pages-demo> 访问此 pages

> 个人对应的 pages 根地址为 `https://<用户名>.gitlab.io/<仓库名>`

## 快速入门
1. 进入 gitlab [新建项目](http://gitlab.com/projects/new)
2. 项目名称填入 `pages-demo`,项目描述填入 `测试 pages demo`,可见等级选择公开,点击 `create-project`
	> 注意不要勾选 `Initialize repository with a README`
3. 执行如下命令

	```bash
	# 克隆此仓库
	git clone git@gitlab.com:chengle297/pages-demo.git
	# 进入此仓库根目录,修改远程仓库 url 为自己创建的仓库地址
	cd pages-demo 
	git remote set-url origin <新创建的仓库地址> 
	# 修改完成后推送到远程
	git push origin master
	```
4. 访问 `http://gitlab.com/pages/<你的用户名>/pages-demo` 即可看到部署成功。

## 原理简述
gitlab 会识别 `.gitlab-ci.yml` 配合进行线上构建。
在构建之前确保本地仓库包含如下内容:
1. `public` 目录该目录会作为部署到静态资源的根目录
2. `gitlab-ci.yml` 构建配置
   
你可以进入 [gitlab-ci.yml](./.gitlab-ci.yml) 理解各字段含义

实际上在日常 pages 部署中你只需要改变 `only` 确定是哪个分支作为部署分支即可
`scripts` 标签目前不支持线上构建,所以你只需要在本地将内容编译到 public 目录即可

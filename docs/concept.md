# gitlab ci 详解

## 快速入门
参考 [gitlab start](https://code.devops.xiaohongshu.com/help/ci/quick_start/README.md)

```yaml
# 定义运行环境
image: ruby:2.1
# 定义 docker 服务
services:
  - postgres

# 定义运行器环境变量，所有脚本均可读取，也可在任务阶段定义
variables:
  DATABASE_URL: "postgres://postgres@postgres/my_database"

# 任务开始前执行脚本
before_script:
  - bundle install

# 任务完成后执行脚本
after_script:
  - rm secrets

# 任务的阶段标签
stages:
  # 构建阶段
  - build
  # 测试阶段
  - test
  # 部署阶段
  - deploy

# 定义任务 job1
job1:
  # 阶段标签
  stage: build
  # 任务执行脚本
  script:
    - execute-script-for-job1
  # 任务执行分支 
  only:
    - master
  # ???
  tags:
    - docker
```



## 核心概念
* **.gitlab-ci.yml** 用于配置 gitlab  ci 如何运行
* **runner** 运行环境 例如 node,php 等支持 docker 镜像
* **job** 任务，完成特定目的在运行环境执行的脚本，，例如初始化，打包，部署等
支持的配置详见 [jobs](https://code.devops.xiaohongshu.com/help/ci/yaml/README.md#jobs) // TODO: 此处需修改


## yml 核心字段
* `image`  docker 镜像
* `services` docker 服务
* `variables` 定义环境变量
* `stages` 定义构建阶段
* `types` stages 的别名，已废弃
* `before_script` 定义任务开始前的执行脚本
* `after_script` 定义任务结束后的执行脚本
* `cache` 定义不同任务之间需要缓存的文件夹，提高任务运行效率

### stages

## 容器使用
[容器使用](https://code.devops.xiaohongshu.com/help/ci/docker/using_docker_images.md)

## 权限控制
详见 [gitlab 权限控制](https://code.devops.xiaohongshu.com/help/user/permissions.md#gitlab-ci)

## 文件下载
* [打包下载](https://code.devops.xiaohongshu.com/help/user/project/pipelines/job_artifacts.md)

## 定时任务
使用 [gitlab 调度自动收集信息](https://code.devops.xiaohongshu.com/help/user/project/pipelines/schedules.md)

## api 列表
使用 [gitlab api 实现本地触发相关操作](https://code.devops.xiaohongshu.com/help/api/README.md)

采用项目服务和其他服务集成触发 [github issue 统计到 jira](https://code.devops.xiaohongshu.com/help/user/project/integrations/project_services.md)


## 资料
* [ ] 详细资料参考 [gitlab ci](https://code.devops.xiaohongshu.com/help/ci/yaml/README.md#jobs)
# 本地安装 gitlab
## 核心概念
1. gitlab 基本架构
2. gitlab executor

## 安装
1. 安装 gitlab services
   
   ```bash
   # 确保安装 docker
   docker pull gitlab/gitlab-ce

   # 启动容器
   docker run --detach --publish 8787:8787 --name gitlab \
   --env GITLAB_OMNIBUS_CONFIG="external_url 'http://localhost:8787';" \
   gitlab/gitlab-ce:latest 
   ```

2. 安装 gitlab runner

  ```bash
  brew install gitlab-runner

  # 输入 service 信息关联 runner
  gitlab-runner login

  # 启动 runner
  gitlab-runner start
  ```

若出现 **this job is stuck**， 参考 [stackoverflow](https://stackoverflow.com/questions/53370840/this-job-is-stuck-because-the-project-doesnt-have-any-runners-online-assigned#answer-53371027) 修复

## 本地运行 .gitlab-ci.yml
可以使用 [gitlab-runner exec](https://docs.gitlab.com/runner/commands/#gitlab-runner-exec) 本地运行配置。


